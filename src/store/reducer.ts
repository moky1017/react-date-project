import { combineReducers } from 'redux'
import { AnyAction } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'
import calendarSlice from './slice/calendarSlice'

const combinedReducer = combineReducers({
  counter: calendarSlice, // [counterSlice.name]: counterSlice
})
const reducer = (state: ReturnType<typeof combinedReducer>, action: AnyAction) => {
  if (action.type === HYDRATE) {
    const nextState = {
      ...state, // use previous state
      ...action.payload, // apply delta from hydration
    }
    return nextState
  } else {
    return combinedReducer(state, action)
  }
}

export default reducer
