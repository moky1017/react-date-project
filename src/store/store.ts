import { configureStore, ThunkAction, Action, AnyAction } from '@reduxjs/toolkit'
import { createWrapper } from 'next-redux-wrapper'
import createSagaMiddleware from 'redux-saga'
import thunk from 'redux-thunk'
import reducer from './reducer'
import rootSaga from './saga/index'

export const makeStore = () => {
  const sagaMiddleware = createSagaMiddleware()
  const store: any = configureStore<{}, AnyAction>({
    reducer,
    middleware: [sagaMiddleware],
    devTools: true,
  })
  store.sagaTask = sagaMiddleware.run(rootSaga)
  return store
}
type Store = ReturnType<typeof makeStore>
export type AppDispatch = Store['dispatch']
export type AppState = ReturnType<Store['getState']>
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, AppState, unknown, Action<string>>
export const wrapper = createWrapper<any>(makeStore, { debug: true })
