import styled from 'styled-components'
import DatePicker from 'react-datepicker'
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth'
import { Button } from '@mui/material'
import 'react-datepicker/dist/react-datepicker.css'

export const DateSettingContainer = styled.div`
  background: #fff;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  position: relative;
  width: calc(150px * 4);
`

export const BasicDateContainer = styled.div`
  margin: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const DatePickerContainer = styled(DatePicker)`
  padding: 5px;
  color: ${(p) => p.theme.border};
  margin-left: 10px;
`

export const DateIcon = styled(CalendarMonthIcon)`
  color: ${(p) => p.theme.border};
`

export const DateButton = styled(Button).attrs(() => ({
  variant: 'outlined',
}))`
  width: 150px;
  font-size: 12px;
  display: flex;
  justify-content: space-around;
`

export const TitleInput = styled.input.attrs((p) => ({
  type: 'text',
  placeholder: 'Title Name',
  value: p.value,
}))`
  width: 150px;
  background: ${(p) => p.theme.bg};
  font-size: 12px;
  border: 1px solid ${(p) => p.theme.border};
  color: black;
  outline: none;
  border-radius: 5px;
  padding: 10px;
  &:focus {
    border: 1px solid ${(p) => p.theme.borderFocus};
  }
  &::placeholder {
    color: ${(p) => p.theme.placeholder};
  }
  :hover {
    border: 1px solid ${(p) => p.theme.borderFocus};
  }
`

export const DetailTextarea = styled.textarea.attrs((p) => ({
  placeholder: 'Detail',
  rows: '4',
  value: p.value,
}))`
  resize: none;
  width: calc(150px * 3 + 20px);
  background: ${(p) => p.theme.bg};
  font-size: 12px;
  border: 1px solid ${(p) => p.theme.border};
  color: black;
  outline: none;
  border-radius: 5px;
  padding: 10px;
  margin: 10px;
  &:focus {
    border: 1px solid ${(p) => p.theme.borderFocus};
  }
  &::placeholder {
    color: ${(p) => p.theme.placeholder};
  }
  &:hover {
    border: 1px solid ${(p) => p.theme.borderFocus};
  }
`
