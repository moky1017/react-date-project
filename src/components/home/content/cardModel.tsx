import React, { useEffect, useState } from 'react'
import { useAppDispatch } from 'src/utils/hooks/reduxHook'
import { editAction } from 'src/store/slice/calendarSlice'
import { IMyDataProps } from 'src/interface/calendarType'
import {
  CardContainer,
  TextContent,
  TitleText,
  DateText,
  EditTextarea,
  ButtonList,
  ButtonItem,
} from './style'

interface IProps {
  data: IMyDataProps
  onClick: () => void
}

const defaultForm: IMyDataProps = {
  id: null,
  startDate: null,
  endDate: null,
  title: '',
  detail: '',
}

const CardModel = (props: IProps) => {
  const { data, onClick } = props
  const [isEdit, setEdit] = useState(false)
  const [form, setForm] = useState<IMyDataProps>(defaultForm)

  useEffect(() => {
    setForm(data)
  }, [data])

  const dispatch = useAppDispatch()

  const handleOnChange = () => {
    dispatch(editAction(form))
    setEdit(false)
  }

  return (
    <CardContainer>
      <TextContent>
        <TitleText> {form?.title}:</TitleText>
        <DateText>
          {form?.startDate} - {form?.endDate}
        </DateText>
      </TextContent>

      {isEdit ? (
        <EditTextarea
          value={form.detail}
          onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) =>
            setForm({ ...form, detail: e.target.value })
          }
        />
      ) : (
        <TextContent> {form?.detail}</TextContent>
      )}

      <ButtonList>
        {isEdit ? (
          <ButtonItem onClick={handleOnChange}>submit</ButtonItem>
        ) : (
          <ButtonItem onClick={() => setEdit(true)}>edit</ButtonItem>
        )}

        <ButtonItem onClick={onClick}>delete</ButtonItem>
      </ButtonList>
    </CardContainer>
  )
}

export default CardModel
