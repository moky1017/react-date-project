import React from 'react'
import HomePageModel from 'src/components/home/index'

const HomePage = () => {
  return <HomePageModel />
}

export default HomePage
